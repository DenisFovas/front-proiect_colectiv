import Vue from "vue";
import Router from "vue-router";

import FrontPage from "./../pages/FrontPage";
import LoginForm from "./../components/LoginForm.vue";

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: "/",
			name: "Home",
			component: FrontPage
		},
		{
			path: "/login",
			name: "Login",
			component: LoginForm
		}
	]
});
