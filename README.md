# Front SPA for AcademicInfo 2.0

Technologies: __[Vue js](https://vuejs.org/)__

## How to run it

```bash
git clone https://gitlab.com/DenisFovas/front-proiect_colectiv.git
cd front-proiect_colectiv
npm serve
```

## CSS Rules

For padding/margin etc.

- if you use pixels, use multypliers of 8
- try to make also a mobile friendly version
	- you can check out examples in the _public/css/main.css_ file

## Documentation

Make for each component, with it's input, if it's need something, how it looks and what role does it take.